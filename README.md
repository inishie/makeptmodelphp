
**makePTmodel**

	単体テストのドライバ作成ツール(PHP用)

---

[TOC]

---

## Usage

	PHPの静的なクラスを関数毎に呼び出すためのドライバモジュールを生成できます。
	生成したい対象のphpファイルを引数に指定して起動します。
	ドラッグ＆ドロップ操作用にバッチファイルを追加しています。
	ex) php "makePTmodel.php" "sample.php" 

	生成されるドライバのファイル名(クラス)は、元のファイルの末尾に｢_pt｣を付与したものです。
	生成例は｢sample｣フォルダに格納しています。


## Note

	・bat, txt は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	　ドライバモジュールのエンコードは UTF-8 になります。
	・検証する環境にオートローダー(オートロード)が無い場合、
	　生成されたドライバモジュールに require_once 関数の追記が必要になります。
	・生成されたドライバモジュールをそのまま利用する場合、
	　｢optionphp｣フォルダ内のクラスが必要になります。
	・指定されたクラスの関数毎に呼び出し用の関数を1件ずつ作成します。
	　生成させたくない関数はドライバ生成前に消しておく必要があります。
	　(検証したいPHPファイルを複写後、複写先のファイルを編集してください)
	・修飾子が｢public static｣以外でも、呼び出せる形式で生成されます。
	・関数の復帰値は考慮せず、ドライバモジュール(テンプレート)は固定生成です。
	　生成後に変更してください。
	　検証用のサンプルとして、 pt_common::assertSame() 関数を用意しています。
	・関数の引数は、変数名の列挙のみ行います。
	　生成後に変更してください。
	・PHPUnitの代用品(劣化版)としての利用を想定しています。
	　統合開発環境のデバッグ起動先にドライバモジュールを指定してください。
	・動的なクラスの場合、期待しない動作になることがあります。
	　テストケースを追加する前に動作検証の実施を推奨します。


---

## References

	The PHP Group
	PHP: Closure::bind - Manual
	https://www.php.net/manual/ja/closure.bind.php


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
