<?php
/**
 * PT実行用共通クラス
 */
class pt_common {

	/**
	 * デバッグ出力
	 * @param object  $data    パラメタ
	 * @param boolean $newLine 改行要否
	 */
	public static function org_print($data, $newLine = true) {

		print_r("\r\n");
		print_r($data);

		$needDump = (is_null($data) || is_bool($data) || (is_string($data) && strlen($data) == 0));
		if ($needDump) {
			var_dump($data);
		}
		if ($newLine) {
			print_r("\r\n");
			print_r("\r\n");
		}
	}

	/**
	 * 判定結果の出力
	 * @param  boolean $isPass 判定結果
	 * @return boolean 判定結果
	 */
	public static function isCheck($isPass) {
		$strPrint = "--- check NG ---";
		if ($isPass) {
			$strPrint = "--- check OK ---";
		}
		self::org_print($strPrint);
		return $isPass;
	}

	/**
	 * 判定結果の出力
	 * @param  object  $val1  左辺
	 * @param  object  $val2  右辺
	 * @param  boolean $isDetail 内容出力の要否(不一致は無条件で出力)
	 * @return boolean 判定結果
	 */
	public static function assertSame($val1, $val2, $isDetail = false) {
		$isPass = self::isCheck($val1 === $val2);
		if ($isDetail || ! $isPass) {
			self::org_print($val1, false);
			self::org_print($val2, false);
		}
		return $isPass;
	}

}

?>