<?php
require_once(__DIR__ . '/pt_boot.php');
require_once(__DIR__ . '/pt_common.php');

/**
 * テスト資産
 * @see sample
 */
class sample_pt {

	function main() {
		try {

			pt_common::org_print("start.");


			$this->test_Sample1();

			$this->test_Sample2();

			$this->test_Sample3();

			$this->test_Sample4();

			$this->test_Sample5();


			pt_common::org_print("end.");

		} cath (Throwbale $e) {
			pt_common::org_print("error.");
			pt_common::org_print($e);
		}
	}


	/**
	 * @see sample::Sample1()
	 */
	function test_Sample1() {
		pt_common::org_print(__METHOD__);

		// private, protected
		Closure::bind(function(){
			$index = 0;
			pt_common::org_print(__METHOD__ . " : " . $index++);

			$ptObj = new sample();
			$rtnData = $ptObj->Sample1();
			pt_common::org_print($rtnData);

		}, $this, 'sample')->__invoke();

	}

	/**
	 * @see sample::Sample2()
	 */
	function test_Sample2() {
		pt_common::org_print(__METHOD__);

		// private, protected
		Closure::bind(function(){
			$index = 0;
			pt_common::org_print(__METHOD__ . " : " . $index++);

			$ptObj = new sample();
			$rtnData = $ptObj->Sample2($aa);
			pt_common::org_print($rtnData);

		}, $this, 'sample')->__invoke();

	}

	/**
	 * @see sample::Sample3()
	 */
	function test_Sample3() {
		pt_common::org_print(__METHOD__);

		// private, protected
		Closure::bind(function(){
			$index = 0;
			pt_common::org_print(__METHOD__ . " : " . $index++);

			$ptObj = new sample();
			$rtnData = $ptObj->Sample3($aa, $bb);
			pt_common::org_print($rtnData);

		}, $this, 'sample')->__invoke();

	}

	/**
	 * @see sample::Sample4()
	 */
	function test_Sample4() {
		pt_common::org_print(__METHOD__);
		// public
		$index = 0;
		pt_common::org_print(__METHOD__ . " : " . $index++);

		$rtnData = sample::Sample4($aa);
		pt_common::org_print($rtnData);

	}

	/**
	 * @see sample::Sample5()
	 */
	function test_Sample5() {
		pt_common::org_print(__METHOD__);
		// public
		$index = 0;
		pt_common::org_print(__METHOD__ . " : " . $index++);

		$rtnData = sample::Sample5();
		pt_common::org_print($rtnData);

	}


}
$obj = new sample_pt();
$obj->main();

?>
