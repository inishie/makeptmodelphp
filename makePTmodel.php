<?php
/**
 * テストドライバのテンプレート生成
 * 指定されたPHPプログラムを関数毎に呼び出すテストドライバを生成する。
 */

	$file = $argv[1];
	$phpData = loadPhpFile($file);
	// print_r($phpData);
	makePtFile($phpData);

/**
 * PHPファイルを解析
 * @param string  $file  ファイル名
 * @return string[]      解析結果([0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列)
 */
function loadPhpFile($file) {
	$text = file_get_contents($file); // read file
	$tokens = token_get_all($text);

	// 冗長な空白を除く
	$validityList = array();
	for ($idx = 0; $idx < count($tokens); $idx++) {
		$token = $tokens[$idx];
		if (! is_array($token)) {
			$validityList[] = $token;
			continue;
		}
		if ($token[0] == 377) {
			continue;
		}
		$tokenNm = token_name($token[0]);
		if ($tokenNm == "T_WHITESPACE") {
			continue;
		}
		$validityList[] = $token;
	}
	// print_r($validityList);

	// 必要情報の収集
	$className = null;
	$rtnList = array();	// [0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列
	$isPublic = true;	//アクセス修飾子のpublic判定
	$isStatic = false;	//修飾子のstatic判定
	$fCount = -1;		 // 直近の連番
	$prmFlg = false;	 // パラメタ有効フラグ
	$prmList = array();  // パラメタ退避先
	for ($idx = 0; $idx < count($validityList); $idx++) {
		$token = $validityList[$idx];
		if (! is_array($token)) {
			if ($prmFlg && ($token == ")")) {
				$rtnList[$fCount][3] = $prmList;
				$prmList = array();  // 初期化
				$prmFlg = false;
			}
			continue;
		}

		$tokenNm = token_name($token[0]);
		$nextToken = $validityList[$idx+1];
		if (is_null($className) && ($tokenNm == "T_CLASS")) {
			$className = $nextToken[1];
		}
		if ($tokenNm == "T_PUBLIC") {
			$isPublic = true;
		}
		if (($tokenNm == "T_PRIVATE") || ($tokenNm == "T_PROTECTED")) {
			$isPublic = false;
		}
		if ($tokenNm == "T_STATIC") {
			$isStatic = true;
		}
		if ($tokenNm == "T_FUNCTION") {
			$fCount += 1;
			$rtn = array();
			$rtn[] = $className;
			$rtn[] = ($isPublic && $isStatic);
			$rtn[] = $nextToken[1];
			$rtnList[] = $rtn;

			$prmList = array();  // 初期化
			$isPublic = true;
			$isStatic = false;
			$prmFlg = true;
		}
		if ($tokenNm == "T_VARIABLE") {
			$prmList[] = $token[1];
		}
	}
	// print_r($rtnList);
	return $rtnList;
}

/**
 * テストドライバの情報を生成
 * @param string[]  $phpData  解析結果([0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列)
 */
function makePtFile(array $phpData) {
	$test_method2 = makeMethod2($phpData);
	$test_method1 = makeMethod1($phpData);
	$outData = makeClass($phpData, $test_method1, $test_method2);
	outputFile($outData, $phpData[0][0]);
}

/**
 * 検証対象を呼び出すロジックを生成
 * @param string[]  $phpData  解析結果([0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列)
 * @return string  ドライバプログラム(method2)
 */
function makeMethod2(array $phpData) {
	$tplHeadData = getTemplateContents("test_method2.txt");
	$tplBodyData1 = getTemplateContents("test_method2public.txt");
	$tplBodyData2 = getTemplateContents("test_method2private.txt");
	$outData = "";
	foreach ($phpData as $row) {
		// パラメタ生成
		$tplBodyData = $tplBodyData2;
		if ($row[1]) {
			// public
			$tplBodyData = $tplBodyData1;
		}
		$argument = "";
		if (is_array($row[3])) {
			$argument = implode(", ", $row[3]);
		}

		$testlogic = str_replace(array('%class%', '%method%', '%argument%'),
					array($row[0], $row[2], $argument),
					$tplBodyData);

		$outData .= str_replace(array('%class%', '%method%', '%testlogic%'),
					array($row[0], $row[2], $testlogic),
					$tplHeadData);
	}
	return $outData;
}

/**
 * 検証対象の関数毎の内部関数を生成
 * @param string[]  $phpData  解析結果([0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列)
 * @return string  ドライバプログラム(method1)
 */
function makeMethod1(array $phpData) {
	$tplData = getTemplateContents("test_method1.txt");
	$outData = "";
	foreach ($phpData as $row) {
		$outData .= str_replace(array('%method%'),
					array($row[2]),
					$tplData);
	}
	return $outData;
}

/**
 * テストドライバのクラスを生成
 * @param string[]  $phpData  解析結果([0]:クラス名, [1]:public有無, [2]関数名, [3]変数名配列)
 * @param string  $test_method1  内部関数
 * @param string  $test_method2  呼び出すロジック
 * @return string  出力ファイル(ドライバプログラム)
 */
function makeClass(array $phpData, $test_method1, $test_method2) {
	$classNm = $phpData[0][0];
	$tplData = getTemplateContents("test_class1.txt");

	$outData = str_replace(array('%class%', '%test_method1%', '%test_method2%'),
				array($classNm, $test_method1, $test_method2),
				$tplData);
	return $outData;
}

/**
 * ファイル出力
 * @param string  $outData  出力ファイル
 * @param string  $classNm  検証クラス名
 */
function outputFile($outData, $classNm) {
	$outputStr = mb_convert_encoding($outData, "UTF-8", "SJIS");
	$filename = __DIR__ . "/" . $classNm . "_pt.php";
	file_put_contents($filename, $outputStr);
}

/** テンプレートファイルのデータ取得 */
function getTemplateContents($tplFile) {
	$path = "template/". $tplFile;
	return getContents($path);
}

/** 相対パスでデータ取得 */
function getContents($path) {
	$content = file_get_contents(__DIR__ . "/" . $path);
	return $content;
}

?>