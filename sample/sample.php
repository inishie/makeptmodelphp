<?php
class sample
{
    /** private, dynamic */
    private function Sample1()
    {
        return 1;
    }

    /** public, dynamic */
    public function Sample2($aa) {
        return 1;
    }

    /** protected, dynamic */
    protected function Sample3(array $aa, $bb) {
        $cc = 10;
        return 1;
    }

    /** public, static */
    public static function Sample4($aa = true) {
        return 1;
    }

    /** no return */
    public static function Sample5() {
    }
}
?>